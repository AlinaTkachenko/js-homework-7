/*1. Метод forEach виконує один раз для кожного елементу вказану функцію.*/
/*2. arr.length=0;*/
/*3. Array.isArray(arr);*/

//Варіант 1
function filterBy(arr,typeOfElement){
    let result = [];
    arr.forEach(function(item,index) {
        if(typeof(item) != typeOfElement){  
            result.push(item);
        }
      });
      return result;
}

let arr = [5,'1','Alina',15,'Marina',null,'Andrey','30',30,{name:'Anna'}];
console.log(arr);
console.log(filterBy(arr,'string'));


//Варіант 2
function filterBy(arr,typeOfElement){
    let result = arr.filter(item => typeof(item) != typeOfElement)
    return result;
};
      

let arr1 = [5,'1','Alina',15,'Marina',null,'Andrey','30',30,{name:'Anna'}];
console.log(arr1);
console.log(filterBy(arr1,'string'));

